import 'dart:async';
import 'dart:io';
import 'package:dio/dio.dart';
// Request 
class Request {
  Request();
  // post Form
  Future<Body> postForm(String url, [FormData form]) async {
    var dio = new Dio();
    try {
      FormData formData = new FormData.from({});
      if (form != null) {
        formData = form;
      }
      var response = await dio.post(url, data: formData);
      return Body(response.statusCode, response.data.toString());
    } on TimeoutException catch (_) {
      return Body(600, "connection timeout");
    } on HandshakeException catch (_) {
      return Body(601, "error handshake");
    } on SocketException catch (_) {
      return Body(602, "error handshake");
    } on DioError catch(e) {
      if (e.response != null) {
       return Body(e.response.statusCode, e.response.data.toString());
      } else{
        return Body(400, e.message);
      }  
    } finally {
      dio.clear();
    }
  }

  Future<Body> get(String url,
      [Map<String, dynamic> params]) async {
    var dio = new Dio();
    try {
      var response = await dio.get(url, queryParameters: params);
      return Body(response.statusCode, response.data.toString());
    } on TimeoutException catch (_) {
      return Body(600, "connection timeout");
    } on HandshakeException catch (_) {
      return Body(601, "error handshake");
    } on SocketException catch (_) {
      return Body(602, "error handshake");
    } on DioError catch(e) {
      if (e.response != null) {
       return Body(e.response.statusCode, e.response.data.toString());
      } else{
        return Body(400, e.message);
      }  
    } finally {
      dio.clear();
    }
  }
}

// Body .
class Body {
  int code;
  String body;
  Body(this.code, this.body);
}