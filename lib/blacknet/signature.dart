import 'dart:typed_data'; 


class Signature {
    Uint8List buffer;
    Signature(this.buffer);
    Signature.empty(){
      this.buffer = Uint8List(64);
    }
    Uint8List bytes(){
      return this.buffer;
    }
    int length(){
      return this.buffer.length;
    }
}