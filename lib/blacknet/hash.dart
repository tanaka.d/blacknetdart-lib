import 'dart:typed_data'; 


class Hash {
    Uint8List buffer;
    Hash(this.buffer);
    Hash.empty(){
      this.buffer = Uint8List(32);
    }
    Uint8List bytes(){
      return this.buffer;
    }
    int length(){
      return this.buffer.length;
    }
}