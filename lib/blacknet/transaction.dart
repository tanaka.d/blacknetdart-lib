import 'dart:typed_data'; 
import 'dart:convert';
import 'package:blacknet_lib/blacknet/utils.dart';
import 'package:blacknet_lib/blacknet/message.dart';
import 'package:blacknet_lib/blacknet/signature.dart';
import 'package:blacknet_lib/blacknet/hash.dart';

class Transaction {
    Signature signature;
    String from;
    int seq; // uint32
    Hash hash;
    int fee; //uint64
    int type; //uint8 
    Uint8List data;
    Transaction(String from, int seq, Hash hash, int fee, int type, Uint8List data){
      this.signature = Signature.empty();
      this.from = Utils.publicKeyToHex(Utils.publicKey(from));
      this.seq = seq;
      this.hash = hash;
      this.fee = fee;
      this.type = type;
      this.data = data;
    }
    serialize(){
      // signature
      Uint8List signature = this.signature.bytes();
      // from
      Uint8List from = Utils.hexToPublicKey(this.from);
      // seq 
      Uint8List seq = Utils.toUint32(this.seq);
      // hash
      Uint8List hash = this.hash.bytes();
      // fee 
      Uint8List fee = Utils.toUint64(this.fee);
      // type
      Uint8List type = Utils.toUint8(this.type);
      // data length
      Uint8List len = encodeVarInt(data.length);
      // buffer
      List<int> bytes = List<int>();
      bytes.addAll(signature.toList());
      bytes.addAll(from.toList());
      bytes.addAll(seq.toList());
      bytes.addAll(hash.toList());
      bytes.addAll(fee.toList());
      bytes.addAll(type.toList());
      bytes.addAll(len.toList());
      bytes.addAll(this.data.toList());
      return Uint8List.fromList(bytes);
    }
    static Transaction derialize(Uint8List arr){
      ByteData bytes = arr.buffer.asByteData();
      Signature s = new Signature(arr.sublist(0, 64));
      Hash h = new Hash(arr.sublist(100, 132));
      Transaction t =  new Transaction(
        Utils.publicKeyToHex(arr.sublist(64,96)),
        bytes.getUint32(96),
        h,
        bytes.getUint64(132),
        bytes.getUint8(140),
        arr.sublist(arr.length - decodeVarInt(arr.sublist(141)))
      );
      t.signature = s;
      return t;
    }
}

class Transfer {
    int amount; //uint64
    String to;
    Message message;
    Transfer(int amount, String to, Message message){
      this.amount = amount;
      this.to = Utils.publicKeyToHex(Utils.publicKey(to));
      this.message = message;
    }
    serialize(){
      // amount
      Uint8List amount = Utils.toUint64(this.amount);
      // to
      Uint8List to = Utils.hexToPublicKey(this.to);
      // message
      Uint8List message = this.message.serialize();
      List<int> bytes = List<int>();
      bytes.addAll(amount.toList());
      bytes.addAll(to.toList());
      bytes.addAll(message.toList());
      return Uint8List.fromList(bytes);
    }
    static Transfer derialize(Uint8List arr){
      ByteData bytes = arr.buffer.asByteData();
      int amount = bytes.getUint64(0);
      String to = Utils.publicKeyToAccount(arr.sublist(8,40));
      return new Transfer(amount, to, Message.derialize(arr.sublist(40)));
    }
}


class Lease {
    int amount; //uint64
    String to;
    Lease(int amount, String to){
      this.amount = amount;
      this.to = Utils.publicKeyToHex(Utils.publicKey(to));
    }
    serialize(){
      // amount
      Uint8List amount = Utils.toUint64(this.amount);
      // to
      Uint8List to = Utils.hexToPublicKey(this.to);
      List<int> bytes = List<int>();
      bytes.addAll(amount.toList());
      bytes.addAll(to.toList());
      return Uint8List.fromList(bytes);
    }
    static Lease derialize(Uint8List arr){
      ByteData bytes = arr.buffer.asByteData();
      int amount = bytes.getUint64(0);
      String to = Utils.publicKeyToAccount(arr.sublist(8,40));
      return new Lease(amount, to);
    }
}

class CancelLease {
    int amount; //uint64
    String to;
    int height; //uint32
    CancelLease(int amount, String to, int height){
      this.amount = amount;
      this.to = Utils.publicKeyToHex(Utils.publicKey(to));
      this.height = height;
    }
    serialize(){
      // amount
      Uint8List amount = Utils.toUint64(this.amount);
      // to
      Uint8List to = Utils.hexToPublicKey(this.to);
      // height
      Uint8List height = Utils.toUint32(this.height);
      List<int> bytes = List<int>();
      bytes.addAll(amount.toList());
      bytes.addAll(to.toList());
      bytes.addAll(height.toList());
      return Uint8List.fromList(bytes);
    }
    static CancelLease derialize(Uint8List arr){
      ByteData bytes = arr.buffer.asByteData();
      int amount = bytes.getUint64(0);
      String to = Utils.publicKeyToAccount(arr.sublist(8,40));
      int height = bytes.getUint32(40);
      return new CancelLease(amount, to, height);
    }
}

class WithdrawFromLease {
    int withdraw; //uint64
    int amount; //uint64
    String to;
    int height; //uint32
    WithdrawFromLease(int withdraw, int amount, String to, int height){
      this.withdraw = withdraw;
      this.amount = amount;
      this.to = Utils.publicKeyToHex(Utils.publicKey(to));
      this.height = height;
    }
    serialize(){
      // withdraw
      Uint8List withdraw = Utils.toUint64(this.withdraw);
      // amount
      Uint8List amount = Utils.toUint64(this.amount);
      // to
      Uint8List to = Utils.hexToPublicKey(this.to);
      // height
      Uint8List height = Utils.toUint32(this.height);
      List<int> bytes = List<int>();
      bytes.addAll(withdraw.toList());
      bytes.addAll(amount.toList());
      bytes.addAll(to.toList());
      bytes.addAll(height.toList());
      return Uint8List.fromList(bytes);
    }
    static WithdrawFromLease derialize(Uint8List arr){
      ByteData bytes = arr.buffer.asByteData();
      int withdraw = bytes.getUint64(0);
      int amount = bytes.getUint64(8);
      String to = Utils.publicKeyToAccount(arr.sublist(16,48));
      int height = bytes.getUint32(48);
      return new WithdrawFromLease(withdraw, amount, to, height);
    }
}